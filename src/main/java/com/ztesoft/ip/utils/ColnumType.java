package com.ztesoft.ip.utils;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import com.xiaoleilu.hutool.CharsetUtil;
import com.xiaoleilu.hutool.Setting;

/**
 * 获取java.sql.Types数字对应名称
 * 
 * @author admin
 *
 */
public class ColnumType {
	// (version 1.8 : 52.0, super bit)

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String a = gettypebyInt(12);
		System.out.println(a);
	}

	public static String gettypebyInt(Integer type) {
		File f = new File("resouce/config/colnumtypes.setting");
		Setting s = new Setting(f, CharsetUtil.UTF_8, false);

		Iterator<?> iter = s.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			Object key = entry.getKey();
			Integer val = Integer.parseInt(entry.getValue().toString());
			if (val == type) {
				return key.toString();
			}
		}

		return "no found";

	}
}
