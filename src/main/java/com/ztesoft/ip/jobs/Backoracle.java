package com.ztesoft.ip.jobs;

import org.slf4j.Logger;

import com.xiaoleilu.hutool.DateUtil;
import com.xiaoleilu.hutool.Log;
import com.ztesoft.ip.utils.BatUtils;

public class Backoracle implements Runnable {
	private static final Logger log = Log.get(Backoracle.class.getSimpleName());
	public static String db;
	public static String user;
	public static String pass;
	public static String path;
	private static String repath = null;

	@Override
	public void run() {
		// TODO Auto-generated method stub
		String b = DateUtil.now().replace(":", "-").replace(" ", "");
		repath = "";
		repath = path + "//" + b + ".dmp";
		log.info("开始备份数据库..........." + "信息" + db + user + pass + repath);
		System.out.println("开始备份数据库...........");
		System.out.println("信息" + db + user + pass + repath);

		BatUtils.backOracle(db, user, pass, repath);
		repath = null;
	}
}
