package com.ztesoft.test.service.utils;

import java.security.MessageDigest;
import java.util.List;

public class MD5UTILS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a = "zhuhuixian,malin,jinping,zhangfang,quaiqing,wangshanshan,macaihong,lixinchun,quxiaolin,wangyonghong,lihongcai,zhurunhua,yangxuelin,suyanfang,xieyu,ningyanhua,lichunyu,wangfeng";
		String b = "luoxiangning,liuxiangwen,zhuhuixian";
		String c = "malin,jinping,zhangfang,quaiqing,wangshanshan,wangfeng,macaihong,lihongcai,zhurunhua,yangxuelin,suyanfang,xieyu,ningyanhua,lichunyu,luoxiangning,liuxiangwen,zhuhuixian,majinze";
		String d = "zhouquanhong,yangxiaochun,zhengxin,zhanglixia,zhangliping,wangping,zhangchunyan";

		String[] names = d.split(",");
		for (String s : names) {
			//System.out.println(s + "--" + MD5(getBASE64("" + s + "111111")));
			System.out.println(MD5(getBASE64("" + s + "111111")));
		}

	}

	// 将 s 进行 BASE64 编码
	public static String getBASE64(String s) {
		if (s == null)
			return null;
		return (new sun.misc.BASE64Encoder()).encode(s.getBytes());
	}

	public final static String MD5(String s) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			byte[] btInput = s.getBytes();
			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(btInput);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
